import os, logging, configparser, nextcord
from nextcord.ext import commands
from Classes.Stormbreaker import Stormbreaker

####################
#    Information   #
####################

__author__ = 'Luis Meireles'
__copyright__ = 'Copyright 2022, Bot Stormbreaker'
__license__ = 'GNU GPL v3'
__version__ = '0.0.1'
__maintainer__ = 'Luis Meireles'
__email__ = 'luis.leandro@meireles.pro'
__status__ = 'dev'

####################
#   LOAD CONFIG    #
####################

## Config logs
try:
    logging.basicConfig(filename='Logs/bots_Stormbreaker.log', encoding='utf-8', format='[%(asctime)s] %(levelname)s: %(message)s', level=logging.DEBUG)

except Exception as e:
    logging.error(e)

# Config file
try:
    ## Load file config.ini
    config = configparser.ConfigParser()
    config.read('Config/config.ini')

    ### Load config in var
    bot_token_api = config['Discord-API']['token']
    bot_id_api = config['Discord-API']['id_discord']
except Exception as e:
    logging.error(e)

####################
#       Run        #
####################

# Gateway intents
intents = nextcord.Intents.default()
intents.members = True
intents.presences = True

# Bot prefix
bot = commands.Bot(description='A Simple Tutorial Bot', intents=intents)

if __name__ == '__main__':
    # try loading cogs
    try:
        # Load extension
        for filename in os.listdir('./Commands'):
            if filename.endswith('.py'):
                bot.load_extension(f'Commands.{filename[: -3]}')
    except Exception as e:
        print(f'Failed to load Cogs: {e}')
   
    @bot.event
    async def on_ready():
        print(f'We have logged in as {bot.user}')

    bot.add_cog(Stormbreaker(bot))
    bot.run(bot_token_api, reconnect=True, log_level=logging.DEBUG)

bot.start()
