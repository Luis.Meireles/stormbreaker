from nextcord.ext import commands

####################
#    Information   #
####################

__author__ = 'Luis Meireles'
__copyright__ = 'Copyright 2022, Bot Stormbreaker'
__license__ = 'GNU GPL v3'
__version__ = '0.0.1'
__maintainer__ = 'Luis Meireles'
__email__ = 'luis.leandro@meireles.pro'
__status__ = 'dev'

####################
#      Classes     #
####################

class Stormbreaker(commands.Cog):
    def __init__(self, bot, logger):
        self.bot = bot
        self.logger = logger

    # Greetings
    @commands.Cog.listener()
    async def on_ready(self):
        self.logger.info(f'Logged in as {self.bot.user} ({self.bot.user.id})')
        

    # Reconnect
    @commands.Cog.listener()
    async def on_resumed(self):
        self.logger.info('Bot has reconnected!')

    # Error Handlers
    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        # Uncomment line 26 for printing debug
        # await ctx.send(error)

        # Unknown command
        if isinstance(error, commands.CommandNotFound):
            await ctx.send('Invalid Command!')

        # Bot does not have permission
        elif isinstance(error, commands.MissingPermissions):
            await ctx.send('Bot Permission Missing!')
