# Takanashi-Souta Bot
<img src="https://i.pinimg.com/736x/4e/d2/31/4ed231a90e8e351c056af2c16f01fb2a.jpg" alt="Takanashi Souta" width="120"/> <img src="https://downloadr2.apkmirror.com/wp-content/uploads/2022/12/82/63a0db01ad14e.png" alt="Takanashi Souta" width="120"/>

![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![Discord](https://img.shields.io/badge/APP-Discord-blueviolet) ![ChatGPT](https://img.shields.io/badge/API-ChatGPT-gree) 
# Install
1. Use the git clone command to download the project from the repository. The repository URL can be found on the project's website
```bash
git clone https://gitlab.com/user/project.git
```
This will create a new directory with the name of the project and download the project files into it.

2. Navigate into the project directory.
```bash
cd project
```

3. Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the required dependencies.
```bashreply
pip install -r requirement.txt
```

4. Go to "Config" folder and copy file config.ini.example to config.ini.
   
5. Change the username, password and server on the file

# Issus
To create an issue thank you for giving me this information.
- Name issus {bug, security, help, ....}/ brief description of the problem
- Python version
- Repo version
- Return error
- Description how it happens

# To Do
- [ ] Bots Discord 
  - [ ] Bot reply
  - [ ] logs bot
  - [ ] Too many requests
- [ ] Commands
  - [ ] Cron
  - [X] Meteo
  - [ ] ChatGPT
  - [ ] Wiki
  - [ ] Youtube
  - [ ] Server
- [ ] Errors
  - [ ] Logs command
  - [ ] Alert Télégramme
