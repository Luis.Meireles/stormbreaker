from nextcord import Interaction, slash_command
from nextcord.ext.commands import Bot, Cog

####################
#    Information   #
####################

__author__ = 'Luis Meireles'
__copyright__ = 'Copyright 2022, Bot Stormbreaker'
__license__ = 'GNU GPL v3'
__version__ = '0.0.1'
__maintainer__ = 'Luis Meireles'
__email__ = 'luis.leandro@meireles.pro'
__status__ = 'dev'

####################
#      Classes     #
####################

class Ping(Cog):
    def __init__(self, bot: Bot, logger) -> None:
        self.bot = bot
        self.logger = logger

    @slash_command(name="ping-bot", description="A simple ping command.",  guild_ids=[954789825356111932])
    async def ping(self, inter: Interaction) -> None:
        await inter.send(f"Pong! {self.bot.latency * 1000:.2f}ms", delete_after=10)


def setup(bot: Bot) -> None:
    bot.add_cog(Ping(bot))